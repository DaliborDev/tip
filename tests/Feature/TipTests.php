<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TipTests extends TestCase
{

    public function tipCreateTest()
    {
        $response = $this->json('POST', '/api/tip/create', ['title' => 'test title', 'description'  =>  'test description']);

        $response
            ->assertStatus(201)
            ->assertJson([
                'title' => 'test title',
            ]);
    }

    public function tipGetTest()
    {
        $db_tip = 
        $response = $this->json('POST', '/api/tip/get', ['guid' => '90c779a0-cff0-11e8-8121-537e7be3f4d8']);

        $response
            ->assertStatus(201)
            ->assertJson([
                'guid' => '90c779a0-cff0-11e8-8121-537e7be3f4d8',
            ]);
    }


    public function tipUpdateTest()
    {
        $db_tip = 
        $response = $this->json('POST', '/api/tip/update', ['guid' => '90c779a0-cff0-11e8-8121-537e7be3f4d8', 'title' => 'updated by unit test']);

        $response
            ->assertStatus(201)
            ->assertJson([
                'title' => 'updated by unit test',
            ]);
    }

    public function tipGetAllTest()
    {
        $response = $this->json('POST', '/api/tip/get/all');

        $response
            ->assertStatus(201);
    }

    public function tipDeleteTest()
    {
        $response = $this->json('POST', '/api/tip/delete', ['guid' => '90c779a0-cff0-11e8-8121-537e7be3f4d8']);

        $response
            ->assertStatus(201)
            ->assertJson([
                'deleted_at'  => now(),
            ]);
    }
}
