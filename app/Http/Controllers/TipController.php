<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Tip;

class TipController extends Controller
{
    /**
     * Get tips.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTip(Request $request)
    {
        $this->validate($request, [
            'guid'  =>  'required|exists:tips,guid'
        ]);

        \Log::info('Get tip: ' . $request->guid);

        return response()->json(Tip::find($request->guid));
    }

        /**
     * Get tips.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        \Log::info('Get all tips');
        
        return response()->json(Tip::get());
    }


    /**
     * Create new tip in db.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string title
     * @param  string description
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' =>  'required',
            'description'   =>  'required'
        ]);

        $tip    =   new Tip();

        $tip->title =   $request->title;

        $tip->description   =   $request->description;

        $tip->save();

        \Log::info('New tip saved, title: ' . $tip->title . ' at: ' . $tip->created_at);

        return response()->json($tip);
    }


    /**
     * Update the specified tip i db.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $guid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[
            'guid'  =>  'required'
        ]);

        \Log::info('Tip: ' . $tip->title .' update');

        $tip    =   Tip::find($request->guid);
        
        if($tip) {
            
            if($request->has('title')) {
                
                $tip->title =   $request->title;
                
                \Log::info('New title: ' . $request->title);
            }

            if($request->has('description')) {

                $tip->description   =   $request->description;

                \Log::info('New description: ' . $request->description);
            }

            $tip->save();

            return response()->json($tip);
        }  

        return response()->json('error: tip not found', 404);
    }

    /**
     * Remove the specified tip from db.
     *
     * @param  int  $uiid
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'guid'  =>  'required|exists:tips,guid'
        ]);

        $tip = Tip::find($request->guid);

        $tip->delete();

        \Log::info('Tip with guid: ' . $tip->guid . ' is deleted');

        return response()->json('tip: ' . $tip->title . ' deleted');
    }
}
