# TiP
Basic tip entry web api.
The api implement secure jwt token authorization.

The main entry point are:
* get tip
* get all tips
* create tip
* update tip
* delete tip, tips are soft deleted

# Installation

* get the project from repo

* run:  git clone https://gitlab.com/DaliborDev/tip.git tip


* To install packages run: composer install

* setup .env mysql credentials so that laravel can connect to db, after run: php artisan config:cache

* create database named tip

* run the migration and seeder:  php artisan migrate --seed

* ready to go

# Authorization

In order to use the api authentication is required. First the tip/api/login routhe should be called
with the user email and password. The user test user is created with seeder:
email:'tip@gmail.com'
password:tipPassword
